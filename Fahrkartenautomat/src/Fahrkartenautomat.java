﻿import java.util.Scanner;

class Fahrkartenautomat {

    public static void main(String[] args) {
        	 
      
       
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       double rückgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       fahrkartenAusgeben();
        
       rückgeldAusgeben(rückgabeBetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein "+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.\n\n" +
               "==========================");
       
       main(args);
    }
    
    
    public static double fahrkartenbestellungErfassen() { 
    	Scanner tastatur = new Scanner(System.in);    	
    	byte anzahlFahrkarten;
    	double zuZahlenderBetrag = 0;
    	byte auswahl;
    	
    	/* 1. Welche Vorteile hat man durch Verwendung der Arrays?
    	 * Man kann jedes einzelne Element über den Index direkt, schnell und einfach ansprechen.
    	 * Und es ist sehr übersichtlich und kompakt. 
    	 * 
    	 * 2. Vor- und Nachteile mit Arrays bzw. ohne Arrays?
    	 * Mit dem Array sind die Daten generell einfacher zu strukturieren, weshalb auch die Datenpflege von vorallem VIELEN Elementen besser und leichter ist.
    	 * 
    	 * Bei wenig Elementen, wie z.B. 2 Tickets lohnt sich das Erstellen eines Arrays eher weniger. Unnötiger Zeitaufwand und extra Speicherplatz.
    	 */
    	
    	
    	String[] fahrkartenbezeichnung = new String[10];
    	fahrkartenbezeichnung[0] = "Einzelfahrschein Berlin AB";
    	fahrkartenbezeichnung[1] = "Einzelfahrschein Berlin BC";
    	fahrkartenbezeichnung[2] = "Einzelfahrschein Berlin ABC";
    	fahrkartenbezeichnung[3] = "Kurzstrecke";
    	fahrkartenbezeichnung[4] = "Tageskarte Berlin AB";
    	fahrkartenbezeichnung[5] = "Tageskarte Berlin BC";
    	fahrkartenbezeichnung[6] = "Tageskarte Berlin ABC";
    	fahrkartenbezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
    	fahrkartenbezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
    	fahrkartenbezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	
    	double[] fahrkartenpreis = new double[10];
    	fahrkartenpreis[0] = 2.90;
    	fahrkartenpreis[1] = 3.30;
    	fahrkartenpreis[2] = 3.60;
    	fahrkartenpreis[3] = 1.90;
    	fahrkartenpreis[4] = 8.60;
    	fahrkartenpreis[5] = 9.00;
    	fahrkartenpreis[6] = 9.60;
    	fahrkartenpreis[7] = 23.50;
    	fahrkartenpreis[8] = 24.30;
    	fahrkartenpreis[9] = 24.90;
    	
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin aus:");
    	System.out.printf("\n%-20s %-40s %-20s\n", "Auswahlnummer", "Ticketart", "Preis in Euro");
    	
    	for(int i = 0; i < fahrkartenpreis.length; i++) {
    		System.out.printf("%-20d %-40s %-20.2f\n", i+1, fahrkartenbezeichnung[i], fahrkartenpreis[i]);
    	}
    	System.out.print("\nIhre Ticketwahl (1 bis 10): ");
        auswahl = tastatur.nextByte();
        while (auswahl < 1 || auswahl > 10) {
        	System.out.println(" >> Ungültige Eingabe! <<");
        	System.out.print("Ihre Ticketwahl (1 bis 10): ");
        	auswahl = tastatur.nextByte();
        }
        
        if (auswahl == 1) {
        	zuZahlenderBetrag = 2.90;
        } else if (auswahl == 2) {
        	zuZahlenderBetrag = 3.30;
        } else if (auswahl == 3) {
        	zuZahlenderBetrag = 3.60;
        } else if (auswahl == 4) {
        	zuZahlenderBetrag = 1.90;
        } else if (auswahl == 5) {
        	zuZahlenderBetrag = 8.60;
        } else if (auswahl == 6) {
        	zuZahlenderBetrag = 9.00;
        } else if (auswahl == 7) {
        	zuZahlenderBetrag = 9.60;
        } else if (auswahl == 8) {
        	zuZahlenderBetrag = 23.50;
        } else if (auswahl == 9) {
        	zuZahlenderBetrag = 24.30;
        } else if (auswahl == 10) {
        	zuZahlenderBetrag = 24.90;
        }
        
        System.out.print("Anzahl der Tickets: ");
        anzahlFahrkarten = tastatur.nextByte();              
        zuZahlenderBetrag *= anzahlFahrkarten;
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	 Scanner tastatur = new Scanner(System.in);  
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	double rückgabebetrag;
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;                                   
        }
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
        return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");    	
    }
    
    public static void rückgeldAusgeben(double rückgabebetrag) {
    	   	
    	
        if(rückgabebetrag > 0.0) {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro ", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 1.999) // 2 EURO-Münzen
            {
         	  System.out.println("2,00 Euro");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 0.999) // 1 EURO-Münzen
            {
         	  System.out.println("1,00 Euro");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.499) // 50 CENT-Münzen
            {
         	  System.out.println("50 Cent");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.199) // 20 CENT-Münzen
            {
         	  System.out.println("20 Cent");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.099) // 10 CENT-Münzen
            {
         	  System.out.println("10 Cent");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.049)// 5 CENT-Münzen
            {
         	  System.out.println("5 Cent");
  	          rückgabebetrag -= 0.05;
            }
        }        
    }
}