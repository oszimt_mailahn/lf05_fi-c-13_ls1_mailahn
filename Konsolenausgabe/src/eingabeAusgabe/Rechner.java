package eingabeAusgabe;

import java.util.Scanner; // Import der Klasse Scanner
public class Rechner {
 
	public static void main(String[] args) { // Hier startet das Programm 
 
		 // Neues Scanner-Objekt myScanner wird erstellt 
		 Scanner myScanner = new Scanner(System.in); 
		 
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
		 
		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt(); 
		 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 
		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt(); 
		 
		 // Die Addition der Variablen zahl1 und zahl2 
		 // wird der Variable ergebnis zugewiesen.
		 int ergebnisplus = zahl1 + zahl2;
		 int ergebnisminus = zahl1 - zahl2;
		 int ergebnismultipliziert = zahl1 * zahl2;
		 double ergebnisdividiert = (double)zahl1 / (double)zahl2;
		 
		 System.out.print("\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisplus);
		 
		 System.out.print("\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisminus); 
		 
		 System.out.print("\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnismultipliziert); 
		 
		 System.out.print("\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisdividiert); 
		 myScanner.close();
	}
	
}