package eingabeAusgabe;

import java.util.Scanner;

public class konsoleneingabeAufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner merleScanner = new Scanner (System.in);
		
		System.out.println("Hallo Benutzer. Wie ist dein Name?");		
		String name = merleScanner.nextLine();
		
		System.out.print("\"");
		System.out.print(name + "\", das ist aber ein cooler Name.");
		
		
		System.out.println(" Und wie alt bist du?");
		int alter = merleScanner.nextInt();
		
		if (alter < 18) {
			System.out.println("Du bist " + alter + ", also noch nicht volljährig.");			
		} else if (alter >= 18) {
			System.out.println("Du bist " + alter + ", also bist du volljährig.");
		}
		
		System.out.println("\nName: " + name);
		System.out.println("Alter: " + alter);
	}

}
