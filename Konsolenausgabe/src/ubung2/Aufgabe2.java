package ubung2;

public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.printf("%-5s", "0!");
		System.out.printf("%-20s", "=");
		System.out.printf("= 1\n");
		
		System.out.printf("%-5s", "1!");
		System.out.printf("%-20s", "= 1");
		System.out.printf("= 1\n");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("%-20s", "= 1 * 2");
		System.out.printf("= 2\n");
		
		System.out.printf("%-5s", "3!");
		System.out.printf("%-20s", "= 1 * 2 * 3");
		System.out.printf("= 6\n");
		
		System.out.printf("%-5s", "4!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4");
		System.out.printf("= 24\n");
		
		System.out.printf("%-5s", "5!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4 * 5");
		System.out.printf("= 120\n");
	}

}
