package ubung2;

public class Temperaturtabelle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		{
			System.out.printf("%-11s" , "Fahrenheit");
			System.out.print("|");
			System.out.printf("%10s\n" , "Celsius");
		}
		
		System.out.println("----------------------");
		
		{
			System.out.printf("%+-11d" , -20);
			System.out.print("|");
			System.out.printf("%10.2f\n" , -28.8889);
		}
		
		{
			System.out.printf("%+-11d" , -10);
			System.out.print("|");
			System.out.printf("%10.2f\n" , -23.3333);
		}
		
		{
			System.out.printf("%+-11d" , 0);
			System.out.print("|");
			System.out.printf("%10.2f\n" , -17.7778);
		}
		
		{
			System.out.printf("%+-11d" , 20);
			System.out.print("|");
			System.out.printf("%10.2f\n" , -6.6667);
		}
		
		{
			System.out.printf("%+-11d" , 30);
			System.out.print("|");
			System.out.printf("%10.2f\n" , -1.1111);
		}
	}

}
