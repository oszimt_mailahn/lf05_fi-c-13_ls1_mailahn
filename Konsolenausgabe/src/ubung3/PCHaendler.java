package ubung3;

import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("was möchten Sie bestellen?");
		String artikel = liesString(myScanner);

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt(myScanner);

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(myScanner);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble(myScanner);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	
	}
	
	public static String liesString(Scanner myScanner) {
		return (myScanner.nextLine());	
	}
	
	public static int liesInt(Scanner myScanner) {
		return (myScanner.nextInt());
	}
	
	public static double liesDouble(Scanner myScanner) {
		return (myScanner.nextDouble());
	}
	
	public static double berechneGesamtnettopreis(int a, double p) {
		return a * p;
	}
	
	public static double berechneGesamtbruttopreis(double n, double m) {
		return n * (1 + m / 100);
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
	}

}