package ubungKontrollstrukturen;

import java.util.Scanner;

public class auswahlstrukturenAufgabe5 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Gewicht (in kg): ");		
		double gewicht = merlescanner.nextDouble();
		
		System.out.println("Gr��e (in m): ");		
		double groe�e = merlescanner.nextDouble();
		
		System.out.println("Geschlecht (m f�r m�nnlich / w f�r weiblich): ");
		String geschlecht = merlescanner.next();
		
		double bmi = gewicht / (groe�e * groe�e);
		
		System.out.println(bmi);
		
		if(geschlecht.equalsIgnoreCase("m")) {			
			if(bmi<20) {
				System.out.println("Sie sind untergewichtig.");			
			}else if(bmi>=20 && bmi<=25) {
				System.out.println("Sie sind normalgewichtig.");
			}else if(bmi>25) {
				System.out.println("Sie sind �bergewichtig.");
			}			
			
		}else if(geschlecht.equalsIgnoreCase("w")) {
			if(bmi<19) {
				System.out.println("Sie sind untergewichtig.");			
			}else if(bmi>=19 && bmi<=24) {
				System.out.println("Sie sind normalgewichtig.");
			}else if(bmi>24) {
				System.out.println("Sie sind �bergewichtig.");
			}			
			
		}
	}

}
