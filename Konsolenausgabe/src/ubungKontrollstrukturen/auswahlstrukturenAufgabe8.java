package ubungKontrollstrukturen;

import java.util.Scanner;

public class auswahlstrukturenAufgabe8 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Jahreszahl an: ");
		
		int jahreszahl = merlescanner.nextInt();
		
		if(jahreszahl < 1582) {
			if(jahreszahl % 4 == 0) {
				System.out.println("Das Jahr " + jahreszahl + " ist ein Schaltjahr.");
			}
		}else {	
			if(jahreszahl % 4 == 0) {
				
				if(jahreszahl % 100 != 0 && jahreszahl % 400 != 0) {
					System.out.println("Das Jahr " + jahreszahl + " ist ein Schaltjahr.");
				}else {
					if((jahreszahl % 100 == 0) && (jahreszahl % 400 == 0)) {
						System.out.println("Das Jahr " + jahreszahl + " ist ein Schaltjahr.");
					}else {
						System.out.println("Das Jahr " + jahreszahl + " ist kein Schaltjahr.");
					}
				}
			}		
		}
		merlescanner.close();
	}
}


