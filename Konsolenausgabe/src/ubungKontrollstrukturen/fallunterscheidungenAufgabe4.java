package ubungKontrollstrukturen;

import java.util.Scanner;

public class fallunterscheidungenAufgabe4 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Bitte nennen Sie zwei Zahlen, mit denen gerechnet werden soll.");
		
		System.out.println("Erste Zahl: ");		
		double ersteZahl = merlescanner.nextDouble();
		
		System.out.println("Zweite Zahl: ");
		double zweiteZahl = merlescanner.nextDouble();
		
		System.out.println("Nennen Sie bitte die Rechenoperation (+, -, *, /): ");
		char rechenzeichen = merlescanner.next().charAt(0);
		
		double ergebnis;
		switch(rechenzeichen) {
			case '+':
				ergebnis = ersteZahl + zweiteZahl;
				System.out.println(ergebnis);
				break;
			case '-':
				ergebnis = ersteZahl - zweiteZahl;
				System.out.println(ergebnis);
				break;
			case '*':
				ergebnis = ersteZahl * zweiteZahl;
				System.out.println(ergebnis);
				break;
			case '/':
				ergebnis = ersteZahl / zweiteZahl;
				System.out.println(ergebnis);
				break;
			default:
				System.out.println("Das war leider kein g�ltiges Rechenzeichen.");
				break;
		}
		merlescanner.close();
	}
}
