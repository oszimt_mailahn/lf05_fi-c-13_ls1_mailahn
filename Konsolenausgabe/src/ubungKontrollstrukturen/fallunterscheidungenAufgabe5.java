package ubungKontrollstrukturen;

import java.util.Scanner;

public class fallunterscheidungenAufgabe5 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Geben Sie an, welche Gr��e berechnet werden soll (R, U oder I): ");
		
		char groe�e = merlescanner.next().charAt(0);
		double ergebnis;
		
		switch(groe�e) {
			case 'R':
				System.out.println("Geben Sie die Werte f�r U und I an.");
				System.out.println("U (Spannung in Volt): ");
				double spannungR = merlescanner.nextDouble();
				System.out.println("I (Strom in Ampere): ");
				double stromR = merlescanner.nextDouble();
				ergebnis = spannungR / stromR;
				System.out.println(ergebnis + " Ohm");
				break;
			case 'U':
				System.out.println("Geben Sie die Werte f�r R und I an.");
				System.out.println("R (Widerstand in Ohm): ");
				double widerstandU = merlescanner.nextDouble();
				System.out.println("I (Strom in Ampere): ");
				double stromU = merlescanner.nextDouble();
				ergebnis = widerstandU * stromU;
				System.out.println(ergebnis + " Volt");
				break;
			case 'I':
				System.out.println("Geben Sie die Werte f�r U und R an.");
				System.out.println("U (Spannung in Volt): ");
				double spannungI = merlescanner.nextDouble();
				System.out.println("R (Widerstand in Ohm): ");
				double widerstandI = merlescanner.nextDouble();
				ergebnis = spannungI / widerstandI;
				System.out.println(ergebnis + " Ampere");
			break;
		}

	}

}
