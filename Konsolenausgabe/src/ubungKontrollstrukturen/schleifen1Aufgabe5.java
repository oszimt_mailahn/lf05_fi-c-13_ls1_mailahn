package ubungKontrollstrukturen;

public class schleifen1Aufgabe5 {

	public static void main(String[] args) {
		
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 10; j++) {
				if (j == 10) {
					System.out.printf(" %-3d\n", i * j);
				}else {
					System.out.printf(" %-3d|", i * j);
				}				
			}
		}
		// schleife zuende

	}

}
