package ubungKontrollstrukturen;

import java.util.Scanner;

public class schleifen1Aufgabe8 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Geben Sie an, wie breit Ihr Quadrat sein soll (ganze Zahl): ");
		
		int groe�e = merlescanner.nextInt();
		
		for(int i = 1; i <= groe�e; i++) {
			for(int j = 1; j <= groe�e; j++)
				if (i == groe�e || i == 1 || j == groe�e || j == 1 ) {
				System.out.print("* ");		  		
			} else {
				System.out.print("  ");	
			}
			System.out.println("");
		}
		// schleife zuende
		merlescanner.close();
	}

}
