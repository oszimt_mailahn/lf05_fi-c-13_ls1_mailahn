package ubungKontrollstrukturen;

import java.util.Scanner;

public class schleifen2Aufgabe6 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Geben Sie ihren Kontostand an: ");
		double geld = merlescanner.nextDouble();
		
		System.out.println("Geben Sie den Zinssatz an: ");
		double zinssatz = merlescanner.nextDouble();
		int zaehler = 0;
		
		while(geld < 1000000) {
			
			geld = geld * (zinssatz + 1);			
			zaehler++;
			System.out.println(geld);
		}
		// while vorbei
		
		System.out.println("Sie ben�tigen " + zaehler + " Jahre um eine Million zu haben.");
		
		System.out.println("M�chten Sie eine weitere Berechnung durchf�hren? (j oder n)");
		
		char abfrage = merlescanner.next().charAt(0);
		
		switch(abfrage) {
		case 'j':
			main(args);
			break;
		case 'n':
			break;			
		}
		
		merlescanner.close();  
	}

}
