package ubungKontrollstrukturen;

import java.util.Scanner;

public class schleifen2Aufgabe8 {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl an: ");
		
		int zahl = merlescanner.nextInt();		
		int zaehler = 0;
		
		for(int i = 1; i <= 10; i++) {			
				for(int k = zaehler; k < zaehler + 10; k++) {
					boolean quersummeRichtig = test(k, zahl);
					boolean zahlenthalten = test2(k, zahl);
					if(k % zahl == 0 && k != 0) {
						System.out.print("* ");
					}else if(quersummeRichtig == true){						
						System.out.print("* ");
					}else if(zahlenthalten == true) {
						System.out.print("* ");
					} else {
						System.out.print(k + " ");
					}
				}
			System.out.println("\n");
			zaehler += 10;			
		}
		merlescanner.close();
	}
	
	public static boolean test(int k, int zahl) {
		boolean wahr = false;
		int wert = k % 10;
		int zwischenwert = k - wert;
		
		int zaehler = 0;
		switch(zwischenwert) {
		case 10:
			zaehler = 1;
			break;
		case 20:
			zaehler = 2;
			break;
		case 30:
			zaehler = 3;
			break;
		case 40:
			zaehler = 4;
			break;
		case 50:
			zaehler = 5;
			break;
		case 60:
			zaehler = 6;
			break;
		case 70:
			zaehler = 7;
			break;
		case 80:
			zaehler = 8;
			break;
		case 90:
			zaehler = 9;
			break;
		}
		int wert2 = (zwischenwert % 10) + zaehler;
				
		int ergebnis = wert + wert2;
		if(ergebnis == zahl) {
			wahr = true;
		}		
		return wahr; 
	}
	
	public static boolean test2(int k, int zahl) {		
		boolean wahr = false;
		int wert = k % 10;
		int zwischenwert = k - wert;
		int wert2 = (zwischenwert % 10);
		
		int ergebnis = wert + wert2;
		if(ergebnis == zahl) {
			wahr = true;
		}		
		return wahr;
	}
	
}
