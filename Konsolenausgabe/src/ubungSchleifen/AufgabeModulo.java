package ubungSchleifen;

public class AufgabeModulo {

	public static void main(String[] args) {
		
		for(int i = 0; i <= 200; i++) {
			if(i % 7 == 0) {
				if (i != 0) {
					System.out.println("Die Zahl " + i + " ist durch 7 teilbar.");
				}
				
			} else if((i % 4 == 0) && (i % 5 != 0)) {
				System.out.println("Die Zahl " + i + " ist nicht durch 5 teilbar, aber durch 4.");
			}
		}

	}

}
