package ubungSchleifen;

import java.util.Scanner;

public class AufgabeQuersumme {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl an, die mindestens dreistellig ist.");
		
		int zahl = merlescanner.nextInt();
		
		int summe = 0;
		
		while (0 != zahl) {
			summe = summe + (zahl % 10);
			
			zahl = zahl / 10;
		}

		System.out.println("Die Quersumme betr�gt: " + summe);
	}

}
