package ubungSchleifen;

import java.util.Scanner;

public class AufgabeTemperaturumrechnung {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		double startwert = merlescanner.nextInt();
		
		System.out.println("Bitte den Endwert in Celsius eingeben: ");
		double endwert = merlescanner.nextInt();
		
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double schrittweite = merlescanner.nextInt();
		
		for (double i = startwert; i <= endwert; i = i + schrittweite) {		
			
			double startwertkopie = startwert;
			double fahrenheitwert = umrechnung(startwertkopie);			
			
			System.out.printf("%20.2f Grad Celsius %20.2f Grad Fahrenheit \n", startwert, fahrenheitwert);			
			startwert += schrittweite;
		}
		
		

	}
	
	public static double umrechnung(double fahrenheitwert){
		fahrenheitwert = fahrenheitwert *1.8 + 32;		
		return fahrenheitwert;
	}

}
