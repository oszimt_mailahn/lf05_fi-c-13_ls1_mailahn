package ubungSchleifen;

import java.util.Scanner;

public class AufgabeZaehlen {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte eine Zahl an, von der runter auf 1 gez�hlt werden soll:");
		
		int n = merlescanner.nextInt();
		
		System.out.println("Es geht los in:");
		for(int i = n; i >= 1; i--) {
			
			System.out.println(i);			
			
			if(i == 1) {
				System.out.println("GO!!");
			}
		}
	}

}
