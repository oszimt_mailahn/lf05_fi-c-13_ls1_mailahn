package ubungSchleifen;

import java.util.Scanner;

public class AufgabeZaehlenWhile {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in); 
		
		System.out.println("Bitte geben Sie eine Zahl an, von der hinauf oder runter gez�hlt werden soll: ");
		
		int zahl = merlescanner.nextInt();
		int i = 1;
		int r = 1;
		while (i <= zahl) {
			System.out.println(i);
			i++;
		}
		
		System.out.println(" ");
		
		while (zahl >= r) {
			System.out.println(zahl);
			zahl--;
		}
		
		// Z�hlweise nebeneinander 1, 2, 3, 4, 5, ...
		/* while (zahl >= r) {
			System.out.print(zahl + ", ");
			zahl--;
		} */
	}

}
