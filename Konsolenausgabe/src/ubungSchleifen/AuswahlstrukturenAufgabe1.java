package ubungSchleifen;

import java.util.Scanner;

public class AuswahlstrukturenAufgabe1 {

	public static void main(String[] args) {
	
		Scanner merlescanner = new Scanner(System.in);
		
		// 1. 
		// Wenn K�hlschrank leer, dann einkaufen, wenn voll, dann nicht einkaufen
		// Wenn Wecker klingelt, dann aufstehen, wenn nicht klingelt, dann weiter schlafen
		
		//2.
		System.out.println("Geben Sie zwei Zahlen an, die miteinander verglichen werden sollen:");
				
		int x = merlescanner.nextInt();
		int y = merlescanner.nextInt();
		
		if(x == y) {
			System.out.println("Die beiden Zahlen sind gleich.");			
		}
		
		if(y > x) {
			System.out.println("Die Zahl " + y + " ist gr��er als " + x + ".");			
		} 
		
		if(x >= y) {
			System.out.println("Die Zahl " + x + " ist gr��er oder gleich " + y + ".");	
		} else {
			System.out.println("Die Zahl " + x + " ist nicht gr��er oder gleich " + y + ".");
		}
	
		
		System.out.println("_______________________________");
		System.out.println("Geben Sie drei Zahlen an, die miteinander verglichen werden sollen:");
		
		x = merlescanner.nextInt();
		y = merlescanner.nextInt();
		int z = merlescanner.nextInt();
		
		if((x > y) && (x > z)) {
			System.out.println("Die Zahl " + x + " ist gr��er als " + y + " und " + z + ".");			
		}
		
		if((z > y) || (z > x)) {
			System.out.println("Die Zahl " + z + " ist " + ((z > x) ? "gr��er als" : "kleiner als") + x + " und ist " + ((z > y) ? "gr��er als" : "kleiner als") + y + ".");			
		}
		
		if((x > y) && (x > z)){
			System.out.println("Die Zahl " + x + " ist die gr��te Zahl.");
		}else if((y > z) && (y > x)) {
			System.out.println("Die Zahl " + y + " ist die gr��te Zahl.");
		}else if((z > y) && (z > x)) {
			System.out.println("Die Zahl " + z + " ist die gr��te Zahl.");		
		}
		
	}
}
