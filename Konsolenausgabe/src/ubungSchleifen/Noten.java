package ubungSchleifen;

import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {
		
		Scanner merlescanner = new Scanner(System.in);
				
		System.out.println("Bitte geben Sie ihre Note an: ");
		
		int note = merlescanner.nextInt();
		
		switch(note) {
		
		case 1 :
			System.out.println("1 = Sehr gut");
			break;
		case 2 :
			System.out.println("2 = Gut");
			break;
		case 3 :
			System.out.println("3 = Befriedigend");
			break;
		case 4 :
			System.out.println("4 = Ausreichend");
			break;
		case 5 :
			System.out.println("5 = Mangelhaft");
			break;
		case 6 :
			System.out.println("6 = Ungenügend");
			break;
		default :
			System.out.println("Bitte geben Sie eine Zahl von 1 bis 6 an.");
			Noten.main(args);
			break;
		}
		
		
	}

}
