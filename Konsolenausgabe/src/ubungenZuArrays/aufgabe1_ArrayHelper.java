package ubungenZuArrays;

public class aufgabe1_ArrayHelper {

	public static void main(String[] args) {
		
	int[] zahlen = {1, 2, 3, 4, 5};	
		
	String zahleninWort = convertArrayToString(zahlen);
	
	System.out.println(zahleninWort);
	}

	public static String convertArrayToString(int[] zahlen) {
		String s = "";
		
		for(int i = 0; i<zahlen.length; i++) {
			if(i < zahlen.length - 1) {
			s = s + zahlen[i] + ", ";
			}else {
				s = s + zahlen[i];
			}
		}		
		return s;
	}	
}
