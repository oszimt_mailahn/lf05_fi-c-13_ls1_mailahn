package ubungenZuArrays;
import java.util.Arrays;

public class aufgabe2_arrayMitMethode {
	
	public static void main(String[] args) {
		
	int[] zahlen = {1, 3, 5, 7, 9, 11};	
	methode(zahlen);
	}
	
	public static void methode(int[] zahlen) {				
		int zaehler = zahlen.length-1;
		for(int i = 0; i < zahlen.length/2; i++) {
			int temp = zahlen[i];					
			zahlen[i] = zahlen[zaehler];
			zahlen[zaehler] = temp;
			zaehler--;			
		}		
			System.out.println(Arrays.toString(zahlen));
	}
		
}

