package ubungenZuArrays;

public class aufgabe2_ungeradeZahlen {

	public static void main(String[] args) {
		
		int[] ungeradeZahlen = new int[10];
		
		int zaehler = 1;
		for(int i = 0; i < ungeradeZahlen.length; i ++) {			
			ungeradeZahlen[i] = zaehler;
			zaehler += 2;
		}

		for(int i = 0; i < ungeradeZahlen.length; i ++) {			
			System.out.println(ungeradeZahlen[i]);
		}
		
	}

}
