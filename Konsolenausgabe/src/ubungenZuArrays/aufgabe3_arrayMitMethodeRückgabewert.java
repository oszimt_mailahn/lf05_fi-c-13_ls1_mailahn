package ubungenZuArrays;

import java.util.Arrays;

public class aufgabe3_arrayMitMethodeRückgabewert {

	public static void main(String[] args) {
		
		int[] zahlen = {1, 3, 5, 7, 9, 10};
		int[] neuezahlen = new int[zahlen.length];
		
		System.out.println(Arrays.toString(methode(zahlen, neuezahlen)));
	}
	
	public static int[] methode(int[] zahlen, int[] neuezahlen) {
		int zaehler = zahlen.length-1;
		for(int i = 0; i < zahlen.length; i++) {			
			neuezahlen[i] = zahlen[zaehler];
			zaehler--;
		}		
		return neuezahlen;
	}
}
