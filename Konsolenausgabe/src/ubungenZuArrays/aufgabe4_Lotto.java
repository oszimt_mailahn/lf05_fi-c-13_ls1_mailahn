package ubungenZuArrays;

public class aufgabe4_Lotto {

	public static void main(String[] args) {
		
		int[] lottoZahlen = {3, 7, 12, 18, 37, 42};
		
		for(int i = 0; i < lottoZahlen.length; i++) {
			System.out.print(lottoZahlen[i] + " " + "\n");
		}
		
		boolean ist_12 = false;
		boolean ist_13 = false;
		
		for(int i = 0; i < lottoZahlen.length; i++) {
			if(lottoZahlen[i] == 12) 
				ist_12 = true;
			if(lottoZahlen[i] == 13) 
				ist_13 = true;
		}	
		
		if(ist_12 == true)
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		else
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");

		if(ist_13 == true)
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		else
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");		
	}

}
