package ubungenZuArrays;

import java.util.Scanner;

public class aufgabe4_temperatur {

	public static void main(String[] args) {		
		Scanner merlescanner = new Scanner(System.in);
		
		System.out.println("Wieviele Zeilen soll die Temperaturtabelle haben?");
		
		int zeilenanzahl = merlescanner.nextInt();
		
		double[][] temperatur = new double[zeilenanzahl][2];
		
		double zaehler = 0.0;
		for(int y = 0; y < 1; y++) {
			for(int x = 0; x < temperatur.length; x++) {				
				temperatur[x][y] = zaehler;						
				double celsius = (5.0/9.0) * (temperatur[x][y] - 32.0);				
				temperatur[x][y+1] = celsius;
	
				System.out.printf("%05.2f %s", temperatur[x][y], "F");
				System.out.printf("%+12.2f %s\n", temperatur[x][y+1], "C");
				zaehler += 10.0;
			}			
		}
		merlescanner.close();
	}
}
